package com.jala;

import java.util.LinkedList;

public class AdjacentGraph{
    private final int vertices;
    private final LinkedList<LinkedList<TelefericoEdge>> list;

    public AdjacentGraph(int vertices) {
        this.vertices = vertices;
        this.list = new LinkedList();
        for (int i = 0; i < vertices; i++) {
            this.list.add(new LinkedList<>());
        }
    }

    public void addEdge(int start, int end, int time) {
        TelefericoEdge telefericoEdge= new TelefericoEdge(start, end, time);
        TelefericoEdge telefericoReverse= new TelefericoEdge(end, start, time);
        this.list.get(start).addFirst(telefericoEdge);
        this.list.get(end).addFirst(telefericoReverse);
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < vertices; i++) {
            if (list.get(i).size() > 0) {
                string.append("v ").append(i).append(" : ");
                for (int j = 0; j < list.get(i).size(); j++) {
                    System.out.println();
                    string.append(list.get(i).get(j).toString()).append(" ");
                }
                string.append("\n");
            }
        }
        return string.toString();
    }

    public LinkedList<TelefericoEdge> findNeighbors(int vertex) {
        return new LinkedList<>(this.list.get(vertex));
    }

    public LinkedList<TelefericoEdge> findNeighborsNeighbors(LinkedList<TelefericoEdge> neighbors) {
        LinkedList<TelefericoEdge> result = new LinkedList<>();
        for (TelefericoEdge telefericoEdge : neighbors) {
            LinkedList<TelefericoEdge> resultNeighbors;
            resultNeighbors = findNeighbors(telefericoEdge.getEnd());
            if(resultNeighbors.size() != 0) {
                for (TelefericoEdge teleferico : resultNeighbors) {
                    if(telefericoEdge.getStart() != teleferico.getEnd()) {
                        TelefericoEdge newTeleferico = new TelefericoEdge(telefericoEdge.getStart(),
                                teleferico.getEnd(), teleferico.getTime() + telefericoEdge.getTime());
                        result.add(newTeleferico);
                    }
                }
            }
        }
        return result;
    }

    public String showNeighbors(LinkedList<TelefericoEdge> telefericos) {
        StringBuilder message = new StringBuilder();
        for (TelefericoEdge connection : telefericos) {
            message.append("From ");
            message.append(connection.getStart());
            message.append(" To ");
            message.append(connection.getEnd());
            message.append(" has a cost of ");
            message.append(connection.getTime());
            message.append(" minutes.");
            message.append("\n");
        }
        return message.toString();
    }
}
