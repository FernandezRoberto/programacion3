package com.jala;

public class TelefericoEdge {
    public int start;
    public int end;
    public int time;
    
    public TelefericoEdge(int start, int end, int time){
        this.start = start;
        this.end = end;
        this.time = time;
    }

    @Override
    public String toString() {
        return "TelefericoEdge{" +
                "start=" + start +
                ", end=" + end +
                ", time=" + time +
                '}';
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public int getTime() {
        return time;
    }
}
