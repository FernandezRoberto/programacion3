package com.jala;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AdjacentGraphTest {
    @Test
    public void testFindNeighborsNeighborsShouldReturnTheExpectedString() {
        AdjacentGraph adjacentGraph = new AdjacentGraph(29);
        adjacentGraph.addEdge(1,2, 1);
        adjacentGraph.addEdge(2,3, 2);
        adjacentGraph.addEdge(3,4, 2);
        adjacentGraph.addEdge(4,5, 2);
        adjacentGraph.addEdge(5,6, 3);
        adjacentGraph.addEdge(5,7, 3);
        adjacentGraph.addEdge(5,8, 3);
        adjacentGraph.addEdge(8,9, 5);
        adjacentGraph.addEdge(9,10, 5);
        adjacentGraph.addEdge(10,11, 5);
        adjacentGraph.addEdge(11,12, 5);
        adjacentGraph.addEdge(12,26, 6);
        adjacentGraph.addEdge(12,13, 7);
        adjacentGraph.addEdge(13,14, 8);
        adjacentGraph.addEdge(14,15, 5);
        adjacentGraph.addEdge(15,16, 4);
        adjacentGraph.addEdge(16,17, 7);
        adjacentGraph.addEdge(18,19, 4);
        adjacentGraph.addEdge(19,20, 6);
        adjacentGraph.addEdge(20,21, 6);
        adjacentGraph.addEdge(21,22, 3);
        adjacentGraph.addEdge(22,23, 5);
        adjacentGraph.addEdge(23,24, 8);
        adjacentGraph.addEdge(23,25, 7);
        adjacentGraph.addEdge(25,26, 9);
        adjacentGraph.addEdge(26,27, 10);
        adjacentGraph.addEdge(27,28, 5);
        LinkedList<TelefericoEdge> neighbors;
        neighbors = adjacentGraph.findNeighbors(4);
        var neighborsNeighbors = adjacentGraph.findNeighborsNeighbors(neighbors);
        var expected = """
                From 4 To 8 has a cost of 5 minutes.
                From 4 To 7 has a cost of 5 minutes.
                From 4 To 6 has a cost of 5 minutes.
                From 4 To 2 has a cost of 4 minutes.
                """;
        assertEquals(expected, adjacentGraph.showNeighbors(neighborsNeighbors));
    }

    @Test
    public void testFindNeighborsShouldReturnTheExpectedString() {
        AdjacentGraph adjacentGraph = new AdjacentGraph(29);
        adjacentGraph.addEdge(1,2, 1);
        adjacentGraph.addEdge(2,3, 2);
        adjacentGraph.addEdge(3,4, 2);
        adjacentGraph.addEdge(4,5, 2);
        adjacentGraph.addEdge(5,6, 3);
        adjacentGraph.addEdge(5,7, 3);
        adjacentGraph.addEdge(5,8, 3);
        adjacentGraph.addEdge(8,9, 5);
        adjacentGraph.addEdge(9,10, 5);
        adjacentGraph.addEdge(10,11, 5);
        adjacentGraph.addEdge(11,12, 5);
        adjacentGraph.addEdge(12,26, 6);
        adjacentGraph.addEdge(12,13, 7);
        adjacentGraph.addEdge(13,14, 8);
        adjacentGraph.addEdge(14,15, 5);
        adjacentGraph.addEdge(15,16, 4);
        adjacentGraph.addEdge(16,17, 7);
        adjacentGraph.addEdge(18,19, 4);
        adjacentGraph.addEdge(19,20, 6);
        adjacentGraph.addEdge(20,21, 6);
        adjacentGraph.addEdge(21,22, 3);
        adjacentGraph.addEdge(22,23, 5);
        adjacentGraph.addEdge(23,24, 8);
        adjacentGraph.addEdge(23,25, 7);
        adjacentGraph.addEdge(25,26, 9);
        adjacentGraph.addEdge(26,27, 10);
        adjacentGraph.addEdge(27,28, 5);
        var neighbors = adjacentGraph.findNeighbors(4);
        var expected = """
                From 4 To 5 has a cost of 2 minutes.
                From 4 To 3 has a cost of 2 minutes.
                """;
        assertEquals(expected, adjacentGraph.showNeighbors(neighbors));
    }
}
