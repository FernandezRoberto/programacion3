package com.fjala;

public class RedBlackNode <T, K extends Comparable<K>>{
    private T value;
    private K key;
    private boolean isRed;
    private RedBlackNode<T, K> left;
    private RedBlackNode<T, K> right;
    private RedBlackNode<T, K> parent;

    public RedBlackNode(T value, K key, RedBlackNode<T, K> parent){
        this.value = value;
        this.key = key;
        this.parent = parent;
        this.isRed = false;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public boolean isRed() {
        return isRed;
    }

    public void setRed(boolean red) {
        isRed = red;
    }

    public RedBlackNode<T, K> getLeft() {
        return left;
    }

    public void setLeft(RedBlackNode<T, K> left) {
        this.left = left;
    }

    public RedBlackNode<T, K> getRight() {
        return right;
    }

    public void setRight(RedBlackNode<T, K> right) {
        this.right = right;
    }

    public RedBlackNode<T, K> getParent() {
        return parent;
    }

    public void setParent(RedBlackNode<T, K> parent) {
        this.parent = parent;
    }
}
