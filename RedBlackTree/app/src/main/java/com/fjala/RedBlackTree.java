package com.fjala;

public class RedBlackTree<T, K extends Comparable<K>> {

    private RedBlackNode<T, K> root;
    private RedBlackNode<T, K> nil;

    public RedBlackTree(RedBlackNode<T, K> root, RedBlackNode<T, K> nil) {
        this.root = root;
        this.nil = nil;
    }

    public void rotateLeft(RedBlackNode<T, K> x) {
        RedBlackNode<T, K> y = x.getRight();
        x.setRight(y.getLeft());
        if (y.getLeft() != nil) {
            y.getLeft().setParent(x);
        }
        y.setParent(x.getParent());
        if (x.getParent() == nil) {
            root = y;
        } else if (x.getParent().getLeft() == x) {
            x.getParent().setLeft(y);
        } else {
            x.getParent().setRight(y);
        }
        y.setLeft(x);
        x.setParent(y);
    }

    public void rotateRight(RedBlackNode<T, K> x) {
        RedBlackNode<T, K> y = x.getLeft();
        x.setLeft(y.getRight());
        if (y.getRight() != nil) {
            y.getRight().setParent(x);
        }
        y.setParent(x.getParent());
        if (x.getParent() == nil) {
            root = y;
        } else if (x.getParent().getRight() == x) {
            x.getParent().setRight(y);
        } else {
            x.getParent().setLeft(y);
        }
        y.setRight(x);
        x.setParent(y);
    }


}
