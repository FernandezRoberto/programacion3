package com.fjala;

import java.util.Calendar;

public class Note {
    private final String name;
    private String description;
    private final String category;
    private Calendar date;

    public Note(String name, String description, String category) {
        this.name = name;
        this.description = description.toLowerCase();
        this.category = category;
        this.date = Calendar.getInstance();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Note{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                ", date=" + date.getTime() +
                '}';
    }
}
