package com.fjala;

public class SearchCategory implements SearchCriteria {
    private final String category;

    public SearchCategory(String category){
        this.category = category;
    }

    @Override
    public boolean containsNote(Note note) {
        return note.getCategory().equalsIgnoreCase(category);
    }
}