package com.fjala;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class EncryptionMorseCode implements EncryptionCriteria{
    BiMap<Character, String> encryptTable;
    BiMap<String, Character> decryptTable;

    public EncryptionMorseCode() {
        encryptTable = HashBiMap.create();
        decryptTable = HashBiMap.create();
        createMorseTable();
    }

    public void createMorseTable() {
        encryptTable.put('a', ".-");
        encryptTable.put('b', "-...");
        encryptTable.put('c', "-.-.");
        encryptTable.put('d', "-..");
        encryptTable.put('e', ".");
        encryptTable.put('f', "..-.");
        encryptTable.put('g', "--.");
        encryptTable.put('h', "....");
        encryptTable.put('i', "..");
        encryptTable.put('j', ".---");
        encryptTable.put('k', "-.-");
        encryptTable.put('l', ".-..");
        encryptTable.put('m', "--");
        encryptTable.put('n', "-.");
        encryptTable.put('o', "---");
        encryptTable.put('p', ".--.");
        encryptTable.put('q', "--.-");
        encryptTable.put('r', ".-.");
        encryptTable.put('s', "...");
        encryptTable.put('t', "-");
        encryptTable.put('u', "..-");
        encryptTable.put('v', "...-");
        encryptTable.put('w', ".--");
        encryptTable.put('x', "-..-");
        encryptTable.put('y', "-.--");
        encryptTable.put('z', "--..");
        encryptTable.put('1', ".----");
        encryptTable.put('2', "..---");
        encryptTable.put('3', "...--");
        encryptTable.put('4', "....-");
        encryptTable.put('5', ".....");
        encryptTable.put('6', "-....");
        encryptTable.put('7', "--...");
        encryptTable.put('8', "---..");
        encryptTable.put('9', "----.");
        encryptTable.put('0', "-----");
        decryptTable = encryptTable.inverse();
    }

    @Override
    public String encryptNote(String description) {
        var sb = new StringBuilder();
        for (int i = 0;i<description.length(); i++) {
            if (description.charAt(i) == ' '){
                sb.append("_ ");
            } else {
                sb.append(encryptTable.get(description.charAt(i)));
                sb.append(' ');
            }
        }
        return sb.toString();
    }

    @Override
    public String decryptNote(String description) {
        var morseLetter = description.split(" ");
        StringBuilder sb = new StringBuilder();
        for (String s : morseLetter) {
             s = s.strip();
            if (s.equals("_")) {
                sb.append(" ");
            } else {
                sb.append(decryptTable.get(s));
            }
        }
        return sb.toString();
    }
}
