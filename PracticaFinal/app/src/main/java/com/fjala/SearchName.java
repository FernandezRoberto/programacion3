package com.fjala;

public class SearchName implements SearchCriteria{
    private final String name;

    public SearchName(String name){
        this.name = name;
    }

    @Override
    public boolean containsNote(Note note) {
        return note.getName().equalsIgnoreCase(name);
    }
}
