package com.fjala;

public interface SearchCriteria {
    boolean containsNote(Note note);
}
