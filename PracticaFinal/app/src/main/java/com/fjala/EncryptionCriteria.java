package com.fjala;

public interface EncryptionCriteria {
    String encryptNote(String description);
    String decryptNote(String description);
}
