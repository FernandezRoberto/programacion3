package com.fjala;

import java.util.ArrayList;
import java.util.List;

public class NoteSearch {
    private final List<Note> allNotes;
    private final List<Note> noteList;

    public NoteSearch(List<Note> allNotes) {
        this.allNotes = allNotes;
        this.noteList = new ArrayList<>();
    }

    public List<Note> searchNote(SearchCriteria criteria) {
        emptyArticleList();
        for (Note note : allNotes) {
            if (criteria.containsNote(note)) {
                noteList.add(note);
            }
        }
        return noteList;
    }

    private void emptyArticleList() {
        this.noteList.clear();
    }
}
