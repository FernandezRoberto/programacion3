package com.fjala;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public class User {
    private final String name;
    private final LinkedList<Note> notes;

    public User(String name) {
        this.name = name;
        this.notes = new LinkedList<>();
    }

    public LinkedList<Note> getNotes() {
        return notes;
    }

    public boolean registerNote(Note note, EncryptionCriteria criteria){
        if(criteria == null){
            return this.notes.add(note);
        } else {
            note.setDescription(criteria.encryptNote(note.getDescription()));
        }
        return this.notes.add(note);
    }

    public Note decryptNote(String name, EncryptionCriteria criteria){
        for (Note note: this.notes) {
            if(note.getName().equals(name)){
                note.setDescription(criteria.decryptNote(note.getDescription()));
                return note;
            }
        }
        return null;
    }

    public Note encryptNote(String name, EncryptionCriteria criteria){
        for (Note note: this.notes) {
            if(note.getName().equals(name)){
                note.setDescription(criteria.encryptNote(note.getDescription()));
                return note;
            }
        }
        return null;
    }

    public boolean saveNote(String name, String description){
        for (Note note : this.notes) {
            if(note.getName().equals(name)){
                note.setDescription(description);
                note.setDate(Calendar.getInstance());
                return true;
            }
        }
        return false;
    }

    public boolean deleteNote(String name){
        for (Note note : this.notes) {
            if(note.getName().equals(name)){
               notes.remove(note);
               return true;
            }
        }
        return false;
    }

    public List<Note> viewLastThreeNotes(){
        List<Note> threeNotes = new LinkedList<>();
        if (notes.size() >= 3) {
            threeNotes.add(notes.get(notes.size() - 1));
            threeNotes.add(notes.get(notes.size() - 2));
            threeNotes.add(notes.get(notes.size() - 3));
        } else {
            return null;
        }
        return threeNotes;
    }

    public int countNotes(){
        return notes.size();
    }
}
