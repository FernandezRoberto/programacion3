package com.fjala;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AdjacentListUndirectedTest {
    @Test
    public void testAddEdgeShouldReturnTheSizeOfTheListOne() {
        AdjacentListUndirected adjList = new AdjacentListUndirected(4);
        adjList.addEdge(0,1);
        var list = adjList.getList();
        assertEquals(list.get(0).size(), 1);
    }

    @Test
    public void testRemoveEdgeShouldReturnTheSizeOfTheListOne() {
        AdjacentListUndirected adjList = new AdjacentListUndirected(5);
        adjList.addEdge(0,1);
        adjList.addEdge(0,2);
        adjList.addEdge(1,1);
        adjList.removeEdge(0,1);
        var list = adjList.getList();
        assertEquals(list.get(0).size(), 1);
    }

    @Test
    public void testAddVerticesShouldReturnTheSizeOfTheListPlusSix() {
        AdjacentListUndirected adjList = new AdjacentListUndirected(4);
        adjList.addEdge(0,1);
        adjList.addEdge(0,2);
        adjList.addEdge(1,1);
        adjList.addVertices();
        var list = adjList.getList();
        assertEquals(list.size(), 5);
    }

    @Test
    public void testRemoveVerticesShouldReturnTheEmptySubList() {
        AdjacentListUndirected adjList = new AdjacentListUndirected(5);
        adjList.addEdge(4,1);
        adjList.addEdge(4,2);
        adjList.addEdge(4,3);
        adjList.removeVertices(4);
        var list = adjList.getList();
        assertEquals(list.get(4).size(), 0);
    }

    @Test
    public void testFindDegreeShouldReturn3() {
        AdjacentListUndirected adjList = new AdjacentListUndirected(6);
        adjList.addEdge(0,4);
        adjList.addEdge(2,4);
        adjList.addEdge(4,5);
        assertEquals(adjList.findDegree(4), 3);
    }
}
