package com.fjala;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AdjacentListDirectedTest {
    @Test
    public void testAddEdgeShouldReturnTheSizeOfTheListOne() {
        AdjacentListDirected adjList = new AdjacentListDirected(4);
        adjList.addEdge(0,1);
        var list = adjList.getList();
        assertEquals(list.get(0).size(), 1);
    }

    @Test
    public void testRemoveEdgeShouldReturnTheSizeOfTheListOne() {
        AdjacentListDirected adjList = new AdjacentListDirected(5);
        adjList.addEdge(0,1);
        adjList.addEdge(0,2);
        adjList.addEdge(1,1);
        adjList.removeEdge(0,1);
        var list = adjList.getList();
        assertEquals(list.get(0).size(), 1);
    }

    @Test
    public void testAddVerticesShouldReturnTheSizeOfTheListPlusSix() {
        AdjacentListDirected adjList = new AdjacentListDirected(5);
        adjList.addEdge(0,1);
        adjList.addEdge(0,2);
        adjList.addEdge(1,1);
        adjList.addVertices();
        var list = adjList.getList();
        assertEquals(list.size(), 6);
    }

    @Test
    public void testRemoveVerticesShouldReturnTheEmptySubList() {
        AdjacentListDirected adjList = new AdjacentListDirected(5);
        adjList.addEdge(0,1);
        adjList.addEdge(0,2);
        adjList.addEdge(1,1);
        adjList.addEdge(4,1);
        adjList.addEdge(4,2);
        adjList.addEdge(4,3);
        adjList.removeVertices(4);
        var list = adjList.getList();
        assertEquals(list.get(4).size(), 0);
    }

    @Test
    public void testFindInDegreeShouldReturn2() {
        AdjacentListDirected adjList = new AdjacentListDirected(6);
        adjList.addEdge(0,2);
        adjList.addEdge(0,4);
        adjList.addEdge(2,4);
        adjList.addEdge(4,5);
        adjList.addEdge(5,1);
        assertEquals(adjList.findInDegree(4), 2);
    }

    @Test
    public void testFindOutDegreeShouldReturn2() {
        AdjacentListDirected adjList = new AdjacentListDirected(6);
        adjList.addEdge(0,2);
        adjList.addEdge(0,4);
        adjList.addEdge(2,4);
        adjList.addEdge(4,5);
        adjList.addEdge(5,1);
        assertEquals(adjList.findOutDegree(4), 1);
    }
}
