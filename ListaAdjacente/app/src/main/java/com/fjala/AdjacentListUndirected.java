package com.fjala;

import java.util.LinkedList;

public class AdjacentListUndirected {
    private int vertices;
    private final LinkedList<LinkedList<Integer>> list;

    public AdjacentListUndirected(int vertices) {
        this.vertices = vertices;
        this.list = new LinkedList();
        for (int i = 0; i < vertices; i++) {
            this.list.add(new LinkedList<>());
        }
    }

    public void addEdge(int i, int j) {
        this.list.get(i).addFirst(j);
        this.list.get(j).addFirst(i);
    }

    public void removeEdge(int i, int j) {
        this.list.get(i).remove(j);
        this.list.get(j).remove(i);
    }

    public void addVertices() {
        this.vertices++;
        this.list.add(new LinkedList<>());
    }

    public void removeVertices(int vertices) {
        this.list.get(vertices).clear();
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < vertices; i++) {
            if (list.get(i).size() > 0) {
                string.append("v ").append(i).append(" : ");
                for (int j = 0; j < list.get(i).size(); j++) {
                    System.out.println();
                    string.append(list.get(i).get(j)).append(" ");
                }
                string.append("\n");
            }
        }
        return string.toString();
    }

    public LinkedList<LinkedList<Integer>> getList() {
        return list;
    }

    public int findDegree(int number) {
        return list.get(number).size();
    }
}
