package com.fjala;

import java.util.LinkedList;

public class AdjacentListDirected {
    private int vertices;
    private final LinkedList<LinkedList<Integer>> list;

    public AdjacentListDirected(int vertices) {
        this.vertices = vertices;
        this.list = new LinkedList();
        for (int i = 0; i < vertices; i++) {
            this.list.add(new LinkedList<>());
        }
    }

    public void addEdge(int i, int j) {
        this.list.get(i).addFirst(j);
    }

    public void removeEdge(int i, int j) {
        this.list.get(i).remove(j);
    }

    public void addVertices() {
        this.vertices++;
        this.list.add(new LinkedList<>());
    }

    public void removeVertices(int vertices) {
        this.list.get(vertices).clear();
    }

    public LinkedList<LinkedList<Integer>> getList() {
        return list;
    }

    public int findInDegree(int number) {
        int inDegree = 0;
        for (LinkedList<Integer> integers : list) {
            for (Integer integer : integers) {
                if (integer == number) {
                    inDegree++;
                }
            }
        }
        return inDegree;
    }

    public int findOutDegree(int number) {
        return list.get(number).size();
    }
}
