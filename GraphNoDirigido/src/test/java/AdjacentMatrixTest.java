import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class AdjacentMatrixTest {
    @Test
    public void testAddEdgeShouldReturnAndAdjacentMatrixWithItsValueTrue() {
        AdjacentMatrix adjMatrix = new AdjacentMatrix(4);
        adjMatrix.addEdge(0,1);
        var matrix = adjMatrix.getMatrix();
        assertTrue(matrix[0][1]);
    }

    @Test
    public void testRemoveEdgeShouldReturnAndAdjacentMatrixWithItsValueFalse() {
        AdjacentMatrix adjMatrix = new AdjacentMatrix(4);
        adjMatrix.addEdge(0,1);
        adjMatrix.removeEdge(0,1);
        var matrix = adjMatrix.getMatrix();
        assertFalse(matrix[0][1]);
    }

    @Test
    public void testSimulationOfAddingAndRemovingEdgesShouldReturnAValidMatrix() {
        boolean[][] simulation = {
                {false, true, true, false},
                {true, false, true, false},
                {true, true, false, true},
                {false, false, true, false}
        };
        AdjacentMatrix adjMatrix = new AdjacentMatrix(4);
        adjMatrix.addEdge(0,1);
        adjMatrix.removeEdge(0,1);
        adjMatrix.addEdge(0,1);
        adjMatrix.addEdge(0,2);
        adjMatrix.addEdge(1,2);
        adjMatrix.addEdge(2,0);
        adjMatrix.addEdge(2,3);
        var matrix = adjMatrix.getMatrix();
        assertEquals(simulation[2][1], matrix[2][1]);
    }

    @Test
    public void testFindDegreeShouldReturn3() {
        AdjacentMatrix adjMatrix = new AdjacentMatrix(4);
        adjMatrix.addEdge(2,0);
        adjMatrix.addEdge(2,1);
        adjMatrix.addEdge(2,3);
        assertEquals(adjMatrix.findDegree(2), 3);
    }

    @Test
    public void testAddVerticesShouldReturnPlusOneMoreVertices() {
        AdjacentMatrix adjMatrix = new AdjacentMatrix(4);
        adjMatrix.addVertices();
        assertEquals(adjMatrix.getVertices(), 5);
    }

    @Test
    public void testRemoveVerticesShouldReturnMinusOneMoreVertices() {
        AdjacentMatrix adjMatrix = new AdjacentMatrix(4);
        adjMatrix.removeVertices(3);
        assertEquals(adjMatrix.getVertices(), 3);
    }
}
