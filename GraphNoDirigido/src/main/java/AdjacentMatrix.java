public class AdjacentMatrix {
    private boolean[][] matrix;
    private int vertices;

    public AdjacentMatrix(int vertices){
        this.vertices = vertices;
        matrix = new boolean[vertices][vertices];
    }

    public void addEdge(int i, int j) {
        matrix[i][j] = true;
        matrix[j][i] = true;
    }

    public void removeEdge(int i, int j) {
        matrix[i][j] = false;
        matrix[j][i] = false;
    }

    public void removeVertices(int i) {
        this.vertices--;
        for (int j = 0; j < vertices; j++) {
            matrix[i][j] = false;
            matrix[j][i] = false;
        }
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i <= vertices; i++) {
            if(i != 0) {
                string.append(i - 1).append(" ");
            } else {
                string.append("    ");
            }
        }
        System.out.println(string.toString());
        System.out.println();
        string.delete(0,string.length());
        for (int i = 0; i < vertices; i++) {
            string.append(i).append("   ");
            for (boolean flag : matrix[i]) {
                string.append(flag ? 1 : 0).append(" ");
            }
            string.append("\n");
        }
        return string.toString();
    }

    public boolean[][] getMatrix() {
        return matrix;
    }

    public void addVertices() {
        this.vertices++;
        boolean[][] matrixCopy = new boolean[vertices][vertices];
        incrementMatrix(this.matrix, matrixCopy);
        this.matrix = new boolean[vertices][vertices];
        this.matrix = matrixCopy;
    }

    private void incrementMatrix(boolean[][] matrix, boolean[][] matrixCopy) {
        for (int i = 0; i < this.matrix.length; i++) {
            System.arraycopy(this.matrix[i], 0, matrixCopy[i], 0, this.matrix.length);
        }
    }

    public int findDegree(int number) {
        int degree = 0;
        for (int i = 0; i < matrix.length; i++) {
            if(matrix[number][i]) {
                degree++;
            }
        }
        return degree;
    }

    public int getVertices() {
        return this.vertices;
    }
}
