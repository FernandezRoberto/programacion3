package com.fjala;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Book {
    private TreeMap<String, BinarySearchTree> bookIndex = new TreeMap<>();

    public void insertNewTerm(String term){
        bookIndex.put(term, new BinarySearchTree());
    }

    public void insertPageTerm(String term, int page){
        if (!bookIndex.containsKey(term)) {
            insertNewTerm(term);
        }
        bookIndex.get(term).insert(page);
    }

    public void printIndex(){
        for (Map.Entry<String, BinarySearchTree> entry : bookIndex.entrySet()) {
            System.out.print(entry.getKey() + ", ");
            List<Integer> inOrderList = new ArrayList<>();
            entry.getValue().traverseInOrder(entry.getValue().getRoot(), inOrderList);
            entry.getValue().printList(inOrderList);
        }
    }
}
