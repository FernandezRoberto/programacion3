package com.fjala;

import java.util.List;

public class BinarySearchTree<T extends Comparable<T>> {
    private Node<T> root;


    public Node getRoot() {
        return this.root;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public void insert(T value) {
        Node<T> newNode = new Node<>(value);
        if (isEmpty()) {
            root = newNode;
        } else {
            Node<T> tempNode = root;
            Node<T> prev = null;
            while (tempNode != null) {
                prev = tempNode;
                if (value.compareTo(tempNode.getValue()) > 0) {
                    tempNode = tempNode.getRight();
                } else {
                    tempNode = tempNode.getLeft();
                }
            }
            if (prev != null) {
                if (value.compareTo(prev.getValue()) < 0) {
                    prev.setLeft(newNode);
                } else {
                    prev.setRight(newNode);
                }
            }
        }
    }

    public void traverseInOrder(Node<T> root, List<T> storageList) {
        if (root != null) {
            traverseInOrder(root.getLeft(), storageList);
            storageList.add(root.getValue());
            traverseInOrder(root.getRight(), storageList);
        }
    }

    public void printList(List<T> list) {
        for (T value : list) {
            System.out.print(value + ",");
        }
        System.out.println();
    }


}
