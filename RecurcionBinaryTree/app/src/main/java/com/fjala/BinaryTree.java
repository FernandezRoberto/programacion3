package com.fjala;

import java.util.List;

public class BinaryTree<T extends Comparable<T>> {
    private Node<T> root;

    public Node getRoot() {
        return this.root;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public void insert(T value) {
        Node<T> newNode = new Node<>(value);
        if (isEmpty()) {
            root = newNode;
        } else {
            Node<T> tempNode = root;
            Node<T> prev = null;
            while (tempNode != null) {
                prev = tempNode;
                if (value.compareTo(tempNode.getValue()) > 0) {
                    tempNode = tempNode.getRight();
                } else {
                    tempNode = tempNode.getLeft();
                }
            }
            if (prev != null) {
                if (value.compareTo(prev.getValue()) < 0) {
                    prev.setLeft(newNode);
                } else {
                    prev.setRight(newNode);
                }
            }
        }
    }

    public void traverseInOrder(Node<T> root, List<T> storageList) {
        if (root != null) {
            traverseInOrder(root.getLeft(), storageList);
            storageList.add(root.getValue());
            traverseInOrder(root.getRight(), storageList);
        }
    }

    public void printList(List<T> list) {
        for (T value : list) {
            System.out.print(value + ",");
        }
        System.out.println();
    }

    public int getNumberLeaf(Node<T> node)
    {
        if (node == null) {
            return 0;
        }
        if (node.getLeft() == null && node.getRight() == null) {
            return 1;
        } else {
            return getNumberLeaf(node.getLeft()) + getNumberLeaf(node.getRight());
        }
    }

    public int findHeight(Node<T> node) {
        if (node == null) {
            return -1;
        } else {
            int leftDepth = findHeight(node.getLeft());
            int rightDepth = findHeight(node.getRight());
            if (leftDepth > rightDepth) {
                return (leftDepth + 1);
            } else {
                return (rightDepth + 1);
            }
        }
    }

    public boolean isBalanced(Node<T> root) {
        if (root == null) {
            return true;
        }
        int leftHeight = findHeight(root.getLeft());
        int rightHeight = findHeight(root.getRight());
        if (Math.abs(leftHeight - rightHeight) <= 1 && isBalanced(root.getRight()) && isBalanced(root.getLeft())){
            return true;
        }
        return false;
    }
}
