package com.fjala;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {
        BinaryTree<Integer> bTree = new BinaryTree<>();
        bTree.insert(5);
        bTree.insert(3);
        bTree.insert(2);
        bTree.insert(4);
        bTree.insert(7);
        bTree.insert(6);
        bTree.insert(8);

        List<Integer> inOrderList = new ArrayList<>();
        bTree.traverseInOrder(bTree.getRoot(), inOrderList);
        bTree.printList(inOrderList);
        System.out.println("There are " + bTree.getNumberLeaf(bTree.getRoot()) + " leaf!");
        System.out.println("The height is " + bTree.findHeight(bTree.getRoot()));
        if (bTree.isBalanced(bTree.getRoot()))
            System.out.println("Tree is balanced");
        else
            System.out.println("Tree is not balanced");
    }
}
