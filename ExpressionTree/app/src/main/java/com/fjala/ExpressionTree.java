package com.fjala;

public class ExpressionTree {

    public Node addOperation(Node root, Node operation, Node right) {
        var aux = root;
        root = operation;
        root.setLeft(aux);
        root.setRight(right);
        return root;
    }

    public int evaluate(Node root) {
        if(root == null) {
            return 0;
        }
        if (root.getLeft() == null && root.getRight() == null) {
            return root.toInteger(root.getValue());
        }
        int left = evaluate(root.getLeft());
        int right = evaluate(root.getRight());
        if (!root.getValue().equals("+")) {
            if (root.getValue().equals("-")) {
                return left - right;
            }
            if (root.getValue().equals("*")) {
                return left * right;
            }
            return left / right;
        } else {
            return left + right;
        }
    }

    public void inOrder(Node root) {
        if(root != null) {
            inOrder(root.getLeft());
            System.out.print(root.getValue());
            inOrder(root.getRight());
        }
    }

    public static void main(String[] args) {
        ExpressionTree et = new ExpressionTree();
        Node root = new Node("+");
        root.setLeft(new Node("3"));
        root.setRight(new Node("*"));
        root.getRight().setLeft(new Node("4"));
        root.getRight().setRight(new Node("1"));
        System.out.print("The expression is: ");
        et.inOrder(root);
        System.out.println("\nThe result of the expression is: " + et.evaluate(root));
        root = et.addOperation(root, new Node("*"), new Node("5"));
        System.out.print("The new expression is: ");
        et.inOrder(root);
        System.out.println("\nThe result of the new expression is: " + et.evaluate(root));
    }
}
