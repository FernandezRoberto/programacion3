package com.fjala;

public class Node {
    private String value;
    private Node left;
    private Node right;

    public Node(String value) {
        this.value = value;
        left = null;
        right = null;
    }

    public String getValue() {
        return value;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public int toInteger(String value) {
        int number = 0;
        if(value.charAt(0) != '-') {
            for (int i = 0; i < value.length(); i++) {
                number = (number * 10) + ((int)value.charAt(i) - 48);
            }
        } else {
            for (int i = 1; i < value.length(); i++) {
                number =number * 10 + ((int)(value.charAt(i))-48);
                number = number * -1;
            }
        }
        return number;
    }
    @Override
    public String toString() {
        return "Node{" + value;
    }
}
