package com.fjala.book;

public class BookIndex extends BookList {

	public void addContent(String name, int page) {
		var termNode = terms.get(name);
		if (termNode != null) {
			Term term = termNode.getValue();
			term.addContent(page);
		} else {
			Term term = new Term(name, page, true);
			terms.insert(term, name);
		}
	}

}
