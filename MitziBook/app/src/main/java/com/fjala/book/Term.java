package com.fjala.book;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Term<T extends Comparable> {

	private String name;
	private List<T> content = new ArrayList<T>();
	private boolean isIndexList;

	public Term(String name, T content, boolean isIndexList) {
		this.name = name;
		this.content.add(content);
		this.isIndexList = isIndexList;
	}

	public Term(String name, List<T> content) {
		this.name = name;
		this.content = content;
	}
	
	private String pagesToString() {
		Collections.sort(content);
		if(isIndexList) {
			int previousPage = (int) content.get(0);
			int countPages = 0;
			StringBuilder pagesSorted = new StringBuilder(String.valueOf(previousPage));
			for (int i = 1; i < content.size(); i++) {
				int currentPage = (int) content.get(i);
				if (currentPage - previousPage > 1) {
					if (countPages > 0) {
						pagesSorted.append(" - ");
						pagesSorted.append(previousPage);
					}
					pagesSorted.append(", ");
					pagesSorted.append(currentPage);
					countPages = 0;
				} else {
					countPages++;
				}
				previousPage = currentPage;
			}
			if (countPages > 0) {
				pagesSorted.append(" - ");
				pagesSorted.append(previousPage);
			}
			return pagesSorted.toString();
		} else {
			StringBuilder definitionSorted = new StringBuilder() ;
			for (int i = 0; i < content.size(); i++) {
				definitionSorted.append(content.get(i));
				//definitionSorted.append("\n");
			}
			return definitionSorted.toString();
		}
	}
	
	@Override
	public String toString() {
		return name + ": " + pagesToString();
		
	}

	public String getName() {
		return name;
	}

	public void addContent(T content) {
		this.content.add(content);
	}

	public List<T>getPages() {
		return this.content;
	}
}
