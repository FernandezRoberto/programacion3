package com.fjala.book;

public class Book {

	private String name;
	private BookIndex index;
	private BookGlossary glossary;

	public Book(String name) {
		this.name = name;
		this.index = new BookIndex();
		this.glossary = new BookGlossary();
	}
	
	public void printBook() {
		System.out.println(name);
		System.out.println("---------------Index---------------");
		index.printTerms();
		System.out.println("---------------Glossary--------------");
		glossary.printTerms();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BookIndex getIndex() {
		return index;
	}

	public void setIndex(BookIndex index) {
		this.index = index;
	}

	public BookGlossary getGlossary() {
		return glossary;
	}

	public void setGlossary(BookGlossary glossary) {
		this.glossary = glossary;
	}
}
