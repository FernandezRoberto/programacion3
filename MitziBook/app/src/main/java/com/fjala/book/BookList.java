package com.fjala.book;

import com.fjala.redblacktree.RedBlackNode;
import com.fjala.redblacktree.RedBlackTree;

import java.util.List;
import java.util.stream.Collectors;

public class BookList {

    protected RedBlackTree<Term, String> terms;
    private static final RedBlackNode<Term, String> NIL = new RedBlackNode<>(null, null, null);

    public BookList() {
        terms = new RedBlackTree<>(NIL);
    }

    public void deleteTerm(String name) {
        terms.delete(name);
    }

    public void updateTerm(String oldName, String newName) {
        var oldNode = terms.get(oldName);
        if (oldNode != null) {
            var oldTerm = oldNode.getValue();
            var newTerm = new Term(newName, oldTerm.getPages());
            terms.delete(oldName);
            terms.insert(newTerm, newName);
        }
    }


    public void printTerms() {
        var termValues = terms.inorder();
        for (var termValue : termValues) {
            System.out.println(termValue.toString());
        }
    }

    public List<Term> searchWordStartsWith(String word) {
        var termValues = terms.inorder();
        return termValues.stream().filter(termValue -> termValue.getName().startsWith(word)).collect(Collectors.toList());
    }
}
