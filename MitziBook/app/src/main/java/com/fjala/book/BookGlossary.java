package com.fjala.book;

public class BookGlossary extends BookIndex {
    public void addTerm(String name, String definition) {
        var termNode = terms.get(name);
        if (termNode != null) {
            Term term = termNode.getValue();
            term.addContent(definition);
        } else {
            Term term = new Term(name, definition, false);
            terms.insert(term, name);
        }
    }
}

