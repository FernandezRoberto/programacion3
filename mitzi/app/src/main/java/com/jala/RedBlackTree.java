package com.jala;

public class RedBlackTree<T, K extends Comparable<K>> {
    private RedBlackNode<T, K> root;
    private RedBlackNode<T, K> nil;

    public RedBlackTree(RedBlackNode<T, K> root) {
        this.root = root;
        this.nil = root.parent;
    }

    public void rotateLeft(RedBlackNode<T, K> x) {
        var y = x.right;
        x.right = y.left;
        if (y.left != nil) {
            y.left.parent = x;
        }
        y.parent = x.parent;
        if (x.parent == nil) {
            root = y;
        } else if (x.parent.left == x) {
            x.parent.left = y;
        } else {
            x.parent.right = y;
        }
        y.left = x;
        x.parent = y;
    }

    public void rotateRight(RedBlackNode<T, K> y) {
        var x = y.left;
        y.left = x.right;
        if (x.right != nil) {
            x.right.parent = y;
        }
        x.parent = y.parent;
        if (y.parent == nil) {
            root = x;
        } else if (y.parent.left == y) {
            y.parent.left = x;
        } else {
            y.parent.right = x;
        }
        x.right = y;
        y.parent = x;
    }


    public void insert(T value, K key) {
        var node = new RedBlackNode<T, K>(value, key, nil, nil, nil);
        insertNode(node);
    }

    public void insertNode(RedBlackNode<T, K> node) {
        if (root == nil) {
            root = node;
            return;
        }
        root = insert(root, node, nil);
        fixAfterInsert(node);
    }

    private RedBlackNode<T, K> insert(RedBlackNode<T, K> tree, RedBlackNode<T, K> node, RedBlackNode<T, K> parent) {
        if (tree == nil) {
            node.parent = parent;
            node.left = nil;
            node.right = nil;
            node.isRed = true;
            return node;
        }
        int cmp = node.key.compareTo(tree.key);
        if (cmp < 0) {
            tree.left = insert(tree.left, node, tree);
        } else {
            tree.right = insert(tree.right, node, tree);
        }
        return tree;
    }

    private void fixAfterInsert(RedBlackNode<T, K> x) {
        while (x.parent.isRed) {
            if (x.parent == x.parent.parent.left) {
                var y = x.parent.parent.right;
                if (y.isRed) {
                    x.parent.isRed = false;
                    y.isRed = false;
                    x.parent.parent.isRed = true;
                    x = x.parent.parent;
                } else {
                    if (x == x.parent.right) {
                        x = x.parent;
                        rotateLeft(x);
                    }
                    x.parent.isRed = false;
                    x.parent.parent.isRed = true;
                    rotateRight(x.parent.parent);
                }
            } else {
                var y = x.parent.parent.left;
                if (y.isRed) {
                    x.parent.isRed = false;
                    y.isRed = false;
                    x.parent.parent.isRed = true;
                    x = x.parent.parent;
                } else {
                    if (x == x.parent.left) {
                        x = x.parent;
                        rotateRight(x);
                    }
                    x.parent.isRed = false;
                    x.parent.parent.isRed = true;
                    rotateLeft(x.parent.parent);
                }
            }
        }
        root.isRed = false;
    }

    public RedBlackNode<T, K> getNil() {
        return nil;
    }

    private RedBlackNode<T, K> getNode(RedBlackNode<T, K> tree, K key) {
        if (tree == nil) {
            return null;
        }
        var limit = tree.getKey().compareTo(key);
        if (limit == 0) {
            return tree;
        } else return limit > 0 ? getNode(tree.left, key) : getNode(tree.right, key);
    }

    private void transplant(RedBlackNode<T, K> x, RedBlackNode<T, K> y) {
        if (x.parent == nil) {
            root = y;
        } else if (x == x.parent.left) {
            x.parent.left = y;
        } else {
            x.parent.right = y;
        }
        y.parent = x.parent;
    }

    public void delete(K key) {
        var node = getNode(root, key);
        if (node != null) {
            delete(node);
        }
    }

    private void delete(RedBlackNode<T, K> x) {
        var y = x;
        RedBlackNode<T, K> z;
        boolean yPreviousColor = y.isRed;
        if (x.left == nil) {
            z = x.right;
            transplant(x, x.right);
        } else if (x.right == nil) {
            z = x.left;
            transplant(x, x.left);
        } else {
            y = min(x.right);
            yPreviousColor = y.isRed;
            z = y.right;
            if (y.parent == x) {
                z.parent = y;
            } else {
                transplant(y, y.right);
                y.right = x.right;
                y.right.parent = y;
            }
            transplant(x, y);
            y.left = x.left;
            y.left.parent = y;
            y.isRed = x.isRed;
        }
        if (!yPreviousColor) {
            fixAfterDelete(z);
        }
    }

    private void fixAfterDelete(RedBlackNode<T, K> x) {
        while (x != root && !x.isRed()) {
            if (x == x.parent.left) {
                var y = x.parent.right;
                if (y.isRed()) {
                    y.isRed = false;
                    x.parent.isRed = true;
                    rotateLeft(x.parent);
                    y = x.parent.right;
                }
                if (!y.left.isRed() && !y.right.isRed()) {
                    y.isRed = true;
                    x = x.parent;
                } else {
                    if (!y.right.isRed()) {
                        y.left.isRed = false;
                        y.isRed = true;
                        rotateRight(y);
                        y = x.parent.right;
                    }
                    y.isRed = x.parent.isRed;
                    x.parent.isRed = false;
                    y.right.isRed = false;
                    rotateLeft(x.parent);
                    x = root;
                }
            } else {
                var y = x.parent.left;
                if (y.isRed()) {
                    y.isRed = false;
                    x.parent.isRed = true;
                    rotateRight(x.parent);
                    y = x.parent.left;
                }
                if (!y.right.isRed() && !y.left.isRed()) {
                    y.isRed = true;
                    x = x.parent;
                } else {
                    if (!y.left.isRed()) {
                        y.right.isRed = false;
                        y.isRed = true;
                        rotateLeft(y);
                        y = x.parent.left;
                    }
                    y.isRed = x.parent.isRed;
                    x.parent.isRed = false;
                    y.right.isRed = false;
                    rotateRight(x.parent);
                    x = root;
                }
            }
        }
        x.isRed = false;
    }

    private RedBlackNode<T, K> min(RedBlackNode<T, K> tree) {
        return tree.left == nil ? tree : min(tree.left);
    }

    public T get(K name) {
        var node = getNode(root, name);
        if (node != null) {
            return node.value;
        }
        return null;
    }
}