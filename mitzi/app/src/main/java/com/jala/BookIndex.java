package com.jala;

public class BookIndex {
    private RedBlackTree<Term, String> terms;

    public BookIndex() {
        this.terms = new RedBlackTree<>(null);
    }

    public void addTerm(String name, int page) {
        Term term = terms.get(name);
        if (term == null) {
            term = new Term(name);
            terms.insert(term, name);
        }
        term.insertPage(page);
    }

    public void deleteTerm(String term) {
        terms.delete(term);
    }

    public Term getTerm(String keyTerm) {
        return this.terms.get(keyTerm);
    }

}
