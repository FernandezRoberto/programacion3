package com.jala;

public class Term {
    private RedBlackTree<Integer, Integer> pageNumbers;
    private String term;

    public Term(String term) {
        this.term = term;
        this.pageNumbers = new RedBlackTree<>(null);
    }

    public void insertPage(int pageNumber) {
        pageNumbers.insert(pageNumber, pageNumber);
    }

    public String getTerm() {
        return this.term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public RedBlackTree<Integer, Integer> getPages() {
        return pageNumbers;
    }

    @Override
    public String toString() {
        return "Term{" +
                "pageNumbers=" + pageNumbers +
                ", term='" + term + '\'' +
                '}';
    }
}

