package com.jala;

public class RedBlackNode<T, K extends Comparable<K>> {
    protected T value;
    protected K key;
    protected RedBlackNode<T, K> left;
    protected RedBlackNode<T, K> right;
    protected RedBlackNode<T, K> parent;
    protected boolean isRed;

    public RedBlackNode(T value, K key, RedBlackNode<T, K> left, RedBlackNode<T, K> right, RedBlackNode<T, K> parent) {
        this.value = value;
        this.key = key;
        this.left = left;
        this.right = right;
        if (left != null)
            this.left.parent = this;
        if (left != null)
            this.right.parent = this;
        this.parent = parent;
    }

    public T getValue() {
        return this.value;
    }

    public K getKey() {
        return this.key;
    }

    public RedBlackNode<T, K> getLeft() {
        return this.left;
    }

    public RedBlackNode<T, K> getRight() {
        return this.right;
    }

    public RedBlackNode<T, K> getParent() {
        return this.parent;
    }

    public boolean isRed() {
        return this.isRed;
    }

    public void setColor(boolean isRed) {
        this.isRed = isRed;
    }
}
